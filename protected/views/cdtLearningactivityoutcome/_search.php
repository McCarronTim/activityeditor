<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>500)); ?>

		<?php echo $form->textFieldRow($model,'createdByUserId',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'lastModifiedByUserId',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'createdDate',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'lastModifiedDate',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
