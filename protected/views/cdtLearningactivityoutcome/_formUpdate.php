<?php
/* @var $this CdtLearningactivityoutcomeController */
/* @var $model CdtLearningactivityoutcome */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cdt-learningactivityoutcome-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo CHtml::link('Cancel', array('CdtLearningactivityoutcome/admin'), array('class'=>'btn btn-danger')); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<b>Code</b><br />
		<?php echo $model->code ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textArea($model,'title',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<b>Created Date</b><br />
		<?php echo $model->createdDate ?>
	</div>

	<div class="row">
		<b>Last Modified Date</b><br />
		<?php echo $model->lastModifiedDate ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->