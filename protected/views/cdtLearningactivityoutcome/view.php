<?php
$this->breadcrumbs=array(
	'Cdt Learningactivityoutcomes'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List CdtLearningactivityoutcome','url'=>array('index')),
array('label'=>'Create CdtLearningactivityoutcome','url'=>array('create')),
array('label'=>'Update CdtLearningactivityoutcome','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete CdtLearningactivityoutcome','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage CdtLearningactivityoutcome','url'=>array('admin')),
);
?>

<h1>View CdtLearningactivityoutcome #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'code',
		'title',
		'createdByUserId',
		'lastModifiedByUserId',
		'createdDate',
		'lastModifiedDate',
),
)); ?>
