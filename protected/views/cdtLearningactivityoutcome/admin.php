<?php
$this->breadcrumbs=array(
	'Cdt Learningactivityoutcomes'=>array('index'),
	'Manage',
);
Yii::import('bootstrap.components.Bootstrap');
?>

<b>
<?php echo CHtml::link('Create new record', array('CdtLearningactivityoutcome/create'), array('class'=>'btn btn-primary')); ?></b>

</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cdt-learningactivityoutcome-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'code',
		'title',		
		'createdDate',
		'lastModifiedDate',		
array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	'template'=>'{update}, {delete}'
	),
),
)); ?>


