<?php
$this->breadcrumbs=array(
	'Cdt Learningactivityoutcomes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List CdtLearningactivityoutcome','url'=>array('index')),
array('label'=>'Manage CdtLearningactivityoutcome','url'=>array('admin')),
);
?>

<h1>Create CdtLearningactivityoutcome</h1>

<?php echo $this->renderPartial('_formCreate', array('model'=>$model)); ?>