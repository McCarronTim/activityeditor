<?php
$this->breadcrumbs=array(
	'Cdt Learningactivityoutcomes'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
?>

	<h1>Update Learning Activity Outcome # <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>