<?php

/**
 * This is the model class for table "cdt_learningactivityoutcome".
 *
 * The followings are the available columns in table 'cdt_learningactivityoutcome':
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property integer $createdByUserId
 * @property integer $lastModifiedByUserId
 * @property string $createdDate
 * @property string $lastModifiedDate
 */
class CdtLearningactivityoutcome extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cdt_learningactivityoutcome';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('createdByUserId, lastModifiedByUserId', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>50),
			array('title', 'length', 'max'=>500),
			array('createdDate, lastModifiedDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, title, createdByUserId, lastModifiedByUserId, createdDate, lastModifiedDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'title' => 'Title',
			'createdByUserId' => 'Created By User',
			'lastModifiedByUserId' => 'Last Modified By User',
			'createdDate' => 'Created Date',
			'lastModifiedDate' => 'Last Modified Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('createdByUserId',$this->createdByUserId);
		$criteria->compare('lastModifiedByUserId',$this->lastModifiedByUserId);
		$criteria->compare('createdDate',$this->createdDate,true);
		$criteria->compare('lastModifiedDate',$this->lastModifiedDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CdtLearningactivityoutcome the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
